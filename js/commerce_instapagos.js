/*! IDEART - 2016
    JS File
    by KWEB
 */
(function ($) {
	$(document).ready(function() {
		if ($('#block-commerce-instapagos-instapagos-voucher').length > 0) {
    		var myString = $('#block-commerce-instapagos-instapagos-voucher').find('.content').text();
    		$('#block-commerce-instapagos-instapagos-voucher').find('.content').text("");
    		string_text = myString.replace(/(\r\n|\n|\r)/gm,"");
    		$('#block-commerce-instapagos-instapagos-voucher').find('.content').html(string_text);    		
	       
            $('#block-commerce-instapagos-instapagos-voucher').insertBefore('#commerce-checkout-form-complete');
        }
	});

})(jQuery);

