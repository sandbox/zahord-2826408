Please read this file and also the INSTALL.txt.  
They contain answers to many common questions.

**Description:
The Commerce Instapagos module provides support functions for the commecer payment process
using the service of https://instapago.com/ in Venezuela 

Implementations are provided using the base the Commerce Paypal WPP Module, to add a base creditcard implementation for payment

**Installation AND Upgrades:
See the INSTALL.txt file.

**Notices:
Commerce Instapagos adds a new payment method and a custom block to display the payment Voucher in the review page (this needs to be added manually), the settings for the payment method are the same used for others payment methods

**Credits:
The original module combined the functionality of Commerce Paypal WPP with custom implementation for Instapago created by Deivi Sanchez https://www.drupal.org/u/zahord

Other suggestions and patches contributed by the Drupal community.

Current maintainers: 
  Deivi Sanchez - https://www.drupal.org/u/zahord

**Changes:
See the CHANGELOG.txt file.

